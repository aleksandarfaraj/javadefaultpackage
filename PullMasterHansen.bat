@ECHO off
set /p "p=Are you sure you want to pull from Master repository [y/n]: "
IF %P% == y ( 
	copy Application backup
	git pull origin hansen

	set /p "p=Press enter to exit: "
)