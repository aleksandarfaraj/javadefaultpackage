@ECHO off
set /p "p=Are you sure you want to pull from Master repository [y/n]: "
IF %P% == y ( 
	git pull origin master

	set /p "p=Press enter to exit: "
)