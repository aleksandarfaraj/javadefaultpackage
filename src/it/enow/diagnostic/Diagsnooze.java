import java.util.Date;
import java.util.HashMap;
public class Diagsnooze {

    public static void main(String[] args) {
        Debugger.enable(args);
        Timer.start("Main");
        Memory.printMemoryUsage();
        Timer.start("Main");
    }

    public static class Debugger {
        private static HashMap<String, Boolean> enabledDebuggers = new HashMap<String, Boolean>();
        /**
        Should be ran with main(Args) array. Will enable all arguments in the array for printing later.
        */
        public static void enable(String[] args) {
            for (String s : args) {
                enabledDebuggers.put(s, true);
            }
        }
        /**
        Prints out a formated string by Object s and the following array.
        @param type Name of the debugger
        @parant s if oArr.length > 1 Formatting string. Else it's going to be printed out with System.out.println(Object o); 
        */
        public static void print(String type, Object s, Object ... oArr) {
            Boolean b = enabledDebuggers.get(type);
            if (b == null) {

            } else if (b) {
                if (oArr.length > 0) {
                    System.out.printf("%-12s: ",type);
                    
                    System.out.printf(s.toString(), oArr);
                    System.out.println();
                } else {
                    System.out.println(s);
                }
            }
        }
    }

    public static class Memory {
        private static final double MEGABYTE = 1024L * 1024L;
        private  static double bytesToMegabytes(double bytes) {
            return bytes / MEGABYTE;
        }
        /**
        Prints out the current memory usage with Debugger printer, "Memory"
        */
        public static void printMemoryUsage() {
            Runtime runtime = Runtime.getRuntime();
            double memory = runtime.totalMemory() - runtime.freeMemory();
            Debugger.print("memory", "Used memory is megabytes: %f" , (memory / MEGABYTE));
        }
    }
    public static class Timer {
        private static HashMap<String, Timer> hmap = new HashMap<String, Timer>();
        private Date start;
        private Date stop;
        private String measuring;

        private Timer(String measuring) {
            this.measuring = measuring;
            start = new Date();
        }
        public static void start(String s) {
            Timer t = hmap.get(s);
            if (t == null)
                t = new Timer(s);
            else {
                t.stop(s);
                t.start = new Date();
            }
            hmap.put(s, t);
        }
        private static void stop(String s) {
            Timer t = hmap.get(s);
            t.stop();
        }
        private void stop() {
            Debugger.print("timer", "[%s]: %d ms", measuring, this.getMs());

        }
        private long getMs() {
            Long now = (new Date()).getTime();
            return now - start.getTime() ;
        }
    }
}